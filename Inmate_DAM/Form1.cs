﻿using Inmate_DAM.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Printing;

namespace Inmate_DAM
{
    public partial class Form1 : Form
    {

        #region Drag-Drop
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        Context _context;
        User user;
        public Login loginForm;
        IEnumerable<Inmate_DAM.Models.FileInfo> currentFiles;

        public Form1()
        {
            _context = new Context();
            user = Session.currentUser;

            InitializeComponent();
        }

        public void GeneratePreview()
        {
            
        }

        public void FillCategories()
        {
            var categories = _context.Files.Where(c => !c.isDeleted).ToList();

            lbxCategories.Items.Clear();
            lbxCategories.Items.Add("All");

            foreach (var category in categories)
            {
                lbxCategories.Items.Add(category);
            }
        }

        public void FillMenu(AuthenticationType userAuth)
        {
            if (userAuth.Name == "Manager" || userAuth.Name == "Admin")
            {
                var fileMenuStrip = new ToolStripMenuItem("File");
                

                if (userAuth.Name == "Admin")
                {
                    var file_import = fileMenuStrip.DropDownItems.Add("Import");
                    file_import.Click += new EventHandler(FileImport);
                }
            }
        }

        public void FileImport(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {
                String path = dialog.FileName; // get name of file
                using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open), new UTF8Encoding())) // do anything you want, e.g. read it
                {
                    var fileStream = new FileImport(reader);
                    fileStream._form = this;
                    if (!fileStream.IsDisposed)
                    {
                        fileStream.Show();
                    }
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            loginForm.Close();
        }

        private void lbxCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshFiles();

            RefreshFilters();
        }

        public void RefreshFiles()
        {
            if (lbxCategories.SelectedItem.ToString() == "All")
            {
                currentFiles = _context.FileInformation.Include(fi => fi.User).Include(fi => fi.FileType).Include(fi => fi.User.AuthenticationType).ToList();
            }
            else
            {
                currentFiles = _context.FileInformation.Include(fi => fi.User).Include(fi => fi.FileType).Include(fi => fi.User.AuthenticationType).Where(fd => fd.FileType.Name == ((Models.File)lbxCategories.SelectedItem).Name).ToList();
            }

            lbxFiles.Items.Clear();

            foreach (var file in currentFiles)
            {
                if (!lbxFiles.Items.Contains(file) && !file.FileType.isDeleted)
                {
                    lbxFiles.Items.Add(file);
                }
            }
        }

        public void RefreshFilters()
        {
            cbxFilter.Items.Clear();
            cbxFilter.Items.Add("All");

            var selectedFile = lbxCategories.SelectedItem as Models.File;

            if (lbxCategories.SelectedIndex <= 0 || cbxFilter.SelectedIndex == 0)
            {
                var metaData = _context.FileData.ToList();

                foreach (var meta in metaData)
                {
                    cbxFilter.Items.Add(meta);
                }
            }
            else if (lbxCategories.SelectedIndex > 0)
            {
                var metaData = _context.Map_FileField.Where(m => m.FileType.Id == selectedFile.Id && m.isLinked == true).Select(m => m.MetaDataField).ToList();

                foreach (var meta in metaData)
                {
                    cbxFilter.Items.Add(meta);
                }
            }
            cbxFilter.SelectedIndex = 0;
        }

        private void lbxFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxFiles.SelectedItem == null)
            {
                return;
            }

            var selectedItem = lbxFiles.SelectedItem as Inmate_DAM.Models.FileInfo;

            lbxProperties.Text = selectedItem.FileName + "\r\n\r\n" +
                "File Type: " + selectedItem.FileType.Name + "\r\n\r\n" +
                "Upload Date: " + selectedItem.UploadDate + "\r\n\r\n" +
                "Path: " + selectedItem.FullPath + "\r\n\r\n" +
                "User: " + selectedItem.User.Username + "\r\n" +
                "UserType: " + selectedItem.User.AuthenticationType.Name + "\r\n\r\n" +
                "MetaData" + "\r\n" + "--------" + "\r\n";

            //Metadata
            var metaFields = _context.Map_FileData.Include(mf => mf.FileData).Where(mf => mf.FileInfo.Id == selectedItem.Id).ToList();

            foreach (var metafield in metaFields)
            {
                lbxProperties.Text += $"{metafield.FileData.Name}: {metafield.Value} \r\n";
            }

            PreviewFile(selectedItem);
        }

        private void tbxSearch_TextChanged(object sender, EventArgs e)
        {
            UpdateFiltered();

#region tests
            //if (lbxCategories.SelectedItem.ToString() == "All")
            //{
            //    var files = _context.FileInformation.Include(fi => fi.User).Include(fi => fi.FileType).Include(fi => fi.User.AuthenticationType).Where(fd => fd.FileName.Contains(tbxSearch.Text)).ToList();
            //    lbxFiles.Items.Clear();
            //    foreach (var file in files)
            //    {
            //        lbxFiles.Items.Add(file);
            //    }
            //}
            //else
            //{
            //    IEnumerable<Models.FileInfo> files = _context.FileInformation.Include(fi => fi.User).Include(fi => fi.FileType).Include(fi => fi.User.AuthenticationType).Where(fd => fd.FileType.Name == ((Models.File)lbxCategories.SelectedItem).Name && fd.FileName.Contains(tbxSearch.Text));
            //    lbxFiles.Items.Clear();

            //    foreach (var file in files)
            //    {
            //        lbxFiles.Items.Add(file);
            //    }
            //}
            #endregion
        }

        public void UpdateFiltered()
        {
            var selectedFilter = cbxFilter.SelectedItem as FileData;
            var selectedCategory = lbxCategories.SelectedItem as Inmate_DAM.Models.File;

            lbxFiles.Items.Clear();
            if (cbxFilter.SelectedIndex < 0)
            {
                return;
            }

            if (cbxFilter.SelectedItem.ToString() == "All")
            {
                //files = _context.FileInformation.Where(fd => fd.FileType.Name == ((Models.File)lbxCategories.SelectedItem).Name && fd.FileName.Contains(tbxSearch.Text)).ToList();

                foreach (var result in currentFiles.Where(c => c.FileName.Contains(tbxSearch.Text)))
                {
                    lbxFiles.Items.Add(result);
                }
            }
            else if ((cbxFilter.SelectedItem.ToString() != "All"))
            {
                if (lbxCategories.SelectedItem.ToString() == "All")
                {
                    var results = _context.Map_FileData.Include(c => c.FileInfo).Include(c => c.FileData).Where(c => c.FileData.Id == selectedFilter.Id && c.Value.Contains(tbxSearch.Text));

                    foreach (var result in results)
                    {
                        lbxFiles.Items.Add(result.FileInfo);
                    }
                }
                else
                {
                    if (_context.Map_FileData.Include(c => c.FileInfo).Include(c => c.FileData).Where(c => c.FileInfo.FileType.Id == selectedCategory.Id && c.FileData.Id == selectedFilter.Id && c.Value.Contains(tbxSearch.Text)).Count() > 0)
                    {
                        var results = _context.Map_FileData.Include(c => c.FileInfo).Include(c => c.FileData).Where(c => c.FileInfo.FileType.Id == selectedCategory.Id && c.FileData.Id == selectedFilter.Id && c.Value.Contains(tbxSearch.Text));


                        foreach (var result in results)
                        {
                            lbxFiles.Items.Add(result.FileInfo);
                        }
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlDrag_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnAddFile_Click(object sender, EventArgs e)
        {
            FileImport(this, e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillCategories();
            FillMenu(user.AuthenticationType);
            lbxCategories.SelectedIndex = 0;
            lbxCategories.DisplayMember = "Name";
            cbxFilter.DisplayMember = "Name";
            cbxFilter.SelectedIndex = 0;
            lblUsername.Text = user.Username;
            lblAuthType.Text = user.AuthenticationType.Name;
            GeneratePreview();
            lbxProperties.Text = "";
        }

        private void btn_ManageCategories_Click(object sender, EventArgs e)
        {
            FileManagement fileManagement = new FileManagement();
            fileManagement._mainForm = this;
            fileManagement.ShowDialog();
        }

        private void cbxFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFiltered();
        }

        private void PreviewFile(Inmate_DAM.Models.FileInfo file)
        {
            string filePath = file.FullPath;

            string[] docuList = new string[] { ".txt" };
            string[] pdf = new string[] { ".pdf" };
            string[] imageList = new string[] { ".png", ".jpeg", ".jpg" };
            string[] videoList = new string[] { ".mov", ".mp4", ".wmv" };

            int type_identifier = -1;
            string ext = Path.GetExtension(filePath);

            foreach (string docuExt in docuList)
            {
                if (docuExt.Equals(ext))
                {
                    type_identifier = 1;
                    break;
                }
            }

            foreach (string imageExt in pdf)
            {
                if (imageExt.Equals(ext))
                {
                    type_identifier = 2;
                    break;
                }
            }

            foreach(string videoExt in videoList)
            {
                if (videoExt.Equals(ext))
                {
                    type_identifier = 3;
                    break;
                }
            }

            foreach (string imageExt in imageList)
            {
                if (imageExt.Equals(ext))
                {
                    type_identifier = 4;
                    break;
                }
            }

            switch (type_identifier)
            {
                case 1: //document
                    richTextBox1.BringToFront();
                    richTextBox1.Text = System.IO.File.ReadAllText(Environment.CurrentDirectory + filePath);
                    break;
                case 2: //pdf
                    var myString = Environment.CurrentDirectory + filePath;
                    webBrowser1.BringToFront();
                    webBrowser1.Navigate(@"file:///" + myString);
                    webBrowser1.Show();
                    break;
                default:
                    pictureBox1.BringToFront();
                    pictureBox1.Image = Properties.Resources.preview_error;
                    break;
                case 3: //video
                    axWindowsMediaPlayer1.BringToFront();
                    axWindowsMediaPlayer1.URL = Environment.CurrentDirectory + filePath;
                    break;
                case 4:
                    pictureBox1.BringToFront();
                    pictureBox1.Image = Image.FromFile(Environment.CurrentDirectory + filePath);
                    break;
            }
        }
    }
}
