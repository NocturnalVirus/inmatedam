﻿namespace Inmate_DAM
{
    partial class FileManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDrag = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.lbxCategories = new System.Windows.Forms.ListBox();
            this.lbxMetaData = new System.Windows.Forms.ListBox();
            this.tbxMetaData = new System.Windows.Forms.TextBox();
            this.btnAddMeta = new System.Windows.Forms.Button();
            this.btnUpdateCategory = new System.Windows.Forms.Button();
            this.tmrFade = new System.Windows.Forms.Timer(this.components);
            this.btnDeleteCategory = new System.Windows.Forms.Button();
            this.chkbxShowDeleted = new System.Windows.Forms.CheckBox();
            this.pnlDrag.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDrag
            // 
            this.pnlDrag.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
            this.pnlDrag.Controls.Add(this.label4);
            this.pnlDrag.Controls.Add(this.label3);
            this.pnlDrag.Controls.Add(this.btnClose);
            this.pnlDrag.Location = new System.Drawing.Point(0, 0);
            this.pnlDrag.Name = "pnlDrag";
            this.pnlDrag.Size = new System.Drawing.Size(500, 50);
            this.pnlDrag.TabIndex = 14;
            this.pnlDrag.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDrag_MouseDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(133, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(242, 35);
            this.label4.TabIndex = 15;
            this.label4.Text = "MetaData Manager";
            this.label4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDrag_MouseDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Trebuchet MS", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(12, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 23);
            this.label3.TabIndex = 14;
            this.label3.Text = "Team 3";
            this.label3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDrag_MouseDown);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(434, 0);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 51);
            this.btnClose.TabIndex = 14;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(59)))), ((int)(((byte)(63)))));
            this.panel1.Controls.Add(this.btnAddCategory);
            this.panel1.Controls.Add(this.chkbxShowDeleted);
            this.panel1.Controls.Add(this.btnDeleteCategory);
            this.panel1.Controls.Add(this.lbxCategories);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 425);
            this.panel1.TabIndex = 15;
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.BackColor = System.Drawing.Color.Turquoise;
            this.btnAddCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCategory.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold);
            this.btnAddCategory.Location = new System.Drawing.Point(17, 69);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(166, 26);
            this.btnAddCategory.TabIndex = 20;
            this.btnAddCategory.Text = "Add Category";
            this.btnAddCategory.UseVisualStyleBackColor = false;
            this.btnAddCategory.Click += new System.EventHandler(this.btnAddCategory_Click);
            // 
            // lbxCategories
            // 
            this.lbxCategories.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(42)))), ((int)(((byte)(45)))));
            this.lbxCategories.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbxCategories.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxCategories.ForeColor = System.Drawing.SystemColors.Window;
            this.lbxCategories.FormattingEnabled = true;
            this.lbxCategories.ItemHeight = 18;
            this.lbxCategories.Location = new System.Drawing.Point(17, 102);
            this.lbxCategories.Margin = new System.Windows.Forms.Padding(2);
            this.lbxCategories.Name = "lbxCategories";
            this.lbxCategories.Size = new System.Drawing.Size(165, 270);
            this.lbxCategories.TabIndex = 3;
            this.lbxCategories.SelectedIndexChanged += new System.EventHandler(this.lbxCategories_SelectedIndexChanged);
            // 
            // lbxMetaData
            // 
            this.lbxMetaData.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxMetaData.FormattingEnabled = true;
            this.lbxMetaData.ItemHeight = 18;
            this.lbxMetaData.Location = new System.Drawing.Point(218, 93);
            this.lbxMetaData.Margin = new System.Windows.Forms.Padding(2);
            this.lbxMetaData.Name = "lbxMetaData";
            this.lbxMetaData.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxMetaData.Size = new System.Drawing.Size(263, 274);
            this.lbxMetaData.TabIndex = 16;
            // 
            // tbxMetaData
            // 
            this.tbxMetaData.Font = new System.Drawing.Font("Trebuchet MS", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMetaData.Location = new System.Drawing.Point(218, 68);
            this.tbxMetaData.Margin = new System.Windows.Forms.Padding(2);
            this.tbxMetaData.Name = "tbxMetaData";
            this.tbxMetaData.Size = new System.Drawing.Size(156, 20);
            this.tbxMetaData.TabIndex = 17;
            // 
            // btnAddMeta
            // 
            this.btnAddMeta.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMeta.Location = new System.Drawing.Point(376, 67);
            this.btnAddMeta.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddMeta.Name = "btnAddMeta";
            this.btnAddMeta.Size = new System.Drawing.Size(103, 22);
            this.btnAddMeta.TabIndex = 18;
            this.btnAddMeta.Text = "Add Meta Field";
            this.btnAddMeta.UseVisualStyleBackColor = true;
            this.btnAddMeta.Click += new System.EventHandler(this.btnAddMeta_Click);
            // 
            // btnUpdateCategory
            // 
            this.btnUpdateCategory.BackColor = System.Drawing.Color.Gold;
            this.btnUpdateCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateCategory.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold);
            this.btnUpdateCategory.Location = new System.Drawing.Point(219, 381);
            this.btnUpdateCategory.Name = "btnUpdateCategory";
            this.btnUpdateCategory.Size = new System.Drawing.Size(262, 26);
            this.btnUpdateCategory.TabIndex = 19;
            this.btnUpdateCategory.Text = "Update";
            this.btnUpdateCategory.UseVisualStyleBackColor = false;
            this.btnUpdateCategory.Click += new System.EventHandler(this.btnUpdateCategory_Click);
            // 
            // btnDeleteCategory
            // 
            this.btnDeleteCategory.BackColor = System.Drawing.Color.Brown;
            this.btnDeleteCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCategory.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold);
            this.btnDeleteCategory.Location = new System.Drawing.Point(17, 380);
            this.btnDeleteCategory.Name = "btnDeleteCategory";
            this.btnDeleteCategory.Size = new System.Drawing.Size(166, 26);
            this.btnDeleteCategory.TabIndex = 21;
            this.btnDeleteCategory.Text = "Delete Category";
            this.btnDeleteCategory.UseVisualStyleBackColor = false;
            this.btnDeleteCategory.Click += new System.EventHandler(this.btnDeleteCategory_Click);
            // 
            // chkbxShowDeleted
            // 
            this.chkbxShowDeleted.AutoSize = true;
            this.chkbxShowDeleted.Font = new System.Drawing.Font("Trebuchet MS", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkbxShowDeleted.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.chkbxShowDeleted.Location = new System.Drawing.Point(18, 52);
            this.chkbxShowDeleted.Name = "chkbxShowDeleted";
            this.chkbxShowDeleted.Size = new System.Drawing.Size(84, 19);
            this.chkbxShowDeleted.TabIndex = 20;
            this.chkbxShowDeleted.Text = "Show Deleted";
            this.chkbxShowDeleted.UseVisualStyleBackColor = true;
            this.chkbxShowDeleted.CheckedChanged += new System.EventHandler(this.chkbxShowDeleted_CheckedChanged);
            // 
            // FileManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(498, 425);
            this.Controls.Add(this.btnUpdateCategory);
            this.Controls.Add(this.btnAddMeta);
            this.Controls.Add(this.tbxMetaData);
            this.Controls.Add(this.lbxMetaData);
            this.Controls.Add(this.pnlDrag);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FileManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Management";
            this.Load += new System.EventHandler(this.FileManagement_Load);
            this.pnlDrag.ResumeLayout(false);
            this.pnlDrag.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlDrag;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ListBox lbxCategories;
        private System.Windows.Forms.ListBox lbxMetaData;
        private System.Windows.Forms.TextBox tbxMetaData;
        private System.Windows.Forms.Button btnAddMeta;
        private System.Windows.Forms.Button btnUpdateCategory;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.Timer tmrFade;
        private System.Windows.Forms.Button btnDeleteCategory;
        private System.Windows.Forms.CheckBox chkbxShowDeleted;
    }
}