﻿using Inmate_DAM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Web.Helpers;
using System.Runtime.InteropServices;

namespace Inmate_DAM
{
    public partial class Login : Form
    {
        #region drag-drop
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        Context _context;

        public Login()
        {
            _context = new Context();

            InitializeComponent();

            tbxUsername.Text = "test01";
            tbxPassword.Text = "test1234";
        }


        private void btnLogin_Click(object sender, EventArgs e)
        {
            User user = new User();
            var verified = false;

            if (_context.Users.Where(u => u.Username == tbxUsername.Text).Count() > 0)
            {
                user = _context.Users.Include(u => u.AuthenticationType).Where(u => u.Username == tbxUsername.Text).FirstOrDefault();
                verified = Crypto.VerifyHashedPassword(user.Password, tbxPassword.Text);
                Session.currentUser = user;
            }

            if (verified)
            {
                Session.MainForm(this);
                this.Hide();
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
