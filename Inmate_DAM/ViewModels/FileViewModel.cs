﻿using Inmate_DAM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.ViewModels
{
    public class FileViewModel
    {
        public string FileName { get; set; }
        public Map_FileData FileData { get; set; }
    }
}
