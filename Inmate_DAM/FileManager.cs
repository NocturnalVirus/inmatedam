﻿using Inmate_DAM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Windows.Forms;

namespace Inmate_DAM
{
    public partial class FileManagement : Form
    {
        #region Drag-Drop
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        Context _context;
        public Form1 _mainForm;

        public FileManagement()
        {
            InitializeComponent();
            _context = new Context();
        }

        private void FileManagement_Load(object sender, EventArgs e)
        {
            var fileCategories = _context.Files.Where(c => !c.isDeleted);
            var metaData = _context.FileData.ToList();
            lbxMetaData.DisplayMember = "Name";
            lbxCategories.DisplayMember = "Name";

            foreach (var file in fileCategories)
            {
                lbxCategories.Items.Add(file);
            }

            foreach (var meta in metaData)
            {
                lbxMetaData.Items.Add(meta);
            }

            lbxCategories.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnAddMeta_Click(object sender, EventArgs e)
        {
            _context.FileData.Add(new FileData() { Name = tbxMetaData.Text });
            _context.SaveChanges();

            tbxMetaData.Text = "";

            lbxMetaData.Items.Clear();

            var metaData = _context.FileData.ToList();

            foreach (var meta in metaData)
            {
                lbxMetaData.Items.Add(meta);
            }
        }

        private void lbxCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbxMetaData.SelectedItems.Clear();

            var selectedCategory = lbxCategories.SelectedItem as File;

            var metaDataFields = _context.Map_FileField.Include(c => c.MetaDataField).Where(c => c.FileType.Id == selectedCategory.Id).Where(c => c.isLinked).Select(c => c.MetaDataField).ToList();

            List<int> indexList = new List<int>();

            foreach (var item in lbxMetaData.Items)
            {
                if (metaDataFields.Contains(item))
                {
                    var index = lbxMetaData.Items.IndexOf(item);
                    indexList.Add(index);
                }
            }

            foreach (var index in indexList)
            {
                lbxMetaData.SetSelected(index, true);
            }

            if (selectedCategory.isDeleted)
            {
                btnDeleteCategory.Text = "Activate Category";
                btnDeleteCategory.BackColor = Color.FromArgb(255,30,150,30);
            }
            else
            {
                btnDeleteCategory.Text = "Deactivate Category";
                btnDeleteCategory.BackColor = Color.FromArgb(255, 150, 30, 30);
            }
        }

        private void btnUpdateCategory_Click(object sender, EventArgs e)
        {
            var selectedFile = lbxCategories.SelectedItem as Inmate_DAM.Models.File;
            List<FileData> metaFields = new List<FileData>();

            var metas = new List<FileData>();

            foreach(FileData item in lbxMetaData.Items)
            {
                metas.Add(item);
            }

            foreach (FileData metaData in metas)
            {
                var index = lbxMetaData.Items.IndexOf(metaData);
                if (lbxMetaData.GetSelected(index))
                {

                    if (_context.Map_FileField.Where(c => c.FileType.Id == selectedFile.Id &&
                     c.MetaDataField.Id == metaData.Id && c.isLinked == false).Count() > 0)
                    {
                        _context.Map_FileField.FirstOrDefault(c => c.FileType.Id == selectedFile.Id && c.MetaDataField.Id == metaData.Id && c.isLinked == false).isLinked = true;
                    }
                    //If it doesn't find -> Add
                    else if (_context.Map_FileField.Where(c => c.FileType.Id == selectedFile.Id &&
                     c.MetaDataField.Id == metaData.Id).Count() == 0)
                    {
                        _context.Map_FileField.Add(new Map_FileField()
                        {
                            FileType = _context.Files.FirstOrDefault(c => c.Id == selectedFile.Id),
                            MetaDataField = _context.FileData.FirstOrDefault(c => c.Id == metaData.Id),
                            isLinked = true
                        });
                    }
                }
                else
                {
                    if (_context.Map_FileField.Where(c => c.FileType.Id == selectedFile.Id &&
                    c.MetaDataField.Id == metaData.Id && c.isLinked == true).Count() > 0)
                    {
                        _context.Map_FileField.FirstOrDefault(c => c.FileType.Id == selectedFile.Id &&
                    c.MetaDataField.Id == metaData.Id && c.isLinked == true).isLinked = false;
                    }
                }
            }
            _context.SaveChanges();
            Session.mainForm.RefreshFilters();
            MessageBox.Show(this, "Updated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pnlDrag_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            AddCategory addCategory = new AddCategory();
            addCategory.fileManagement = this;
            addCategory.ShowDialog();
        }

        private void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            _context.Dispose();
            _context = new Context();

            var selectedCategory = lbxCategories.SelectedItem as Inmate_DAM.Models.File;

            if (!selectedCategory.isDeleted)
            {
                _context.Files.FirstOrDefault(c => c.Id == selectedCategory.Id).isDeleted = true;
            }
            else
            {
                _context.Files.FirstOrDefault(c => c.Id == selectedCategory.Id).isDeleted = false;
            }
            _context.SaveChanges();

            ToggleShowDeleted();
        }

        public void ToggleShowDeleted()
        {
            lbxCategories.Items.Clear();
            IEnumerable<File> fileCategories;


            if (!chkbxShowDeleted.Checked)
            {
                fileCategories = _context.Files.Where(c => !c.isDeleted).ToList();
            }
            else
            {
                fileCategories = _context.Files.ToList();
            }

            foreach(File cat in fileCategories)
            {
                if (cat.isDeleted)
                {
                    if (!cat.Name.Contains(" - Del"))
                    {
                        cat.Name = cat.Name + " - Del";
                    }
                }
                else
                {
                    if (cat.Name.Contains(" - Del"))
                    {
                    var cleanName = cat.Name.Split(new char[] { '-' }).FirstOrDefault().Trim();
                    
                    cleanName.Trim();
                    cat.Name = cleanName;
                    }
                }
            }

            foreach (var file in fileCategories)
            {
                lbxCategories.Items.Add(file);
            }
        }

        private void chkbxShowDeleted_CheckedChanged(object sender, EventArgs e)
        {
            ToggleShowDeleted();
            lbxCategories.SelectedIndex = 0;
        }
    }
}
#region Tests
                //foreach(FileData meta in lbxMetaData.Items)
                //{
                //    metaFields.Add(meta);
                //}

                //var mappedFieldsInDb = _context.Map_FileField.Include(c => c.FileType).Include(c => c.MetaDataField).Where(c => c.FileType.Id == selectedFile.Id);

                //foreach (Map_FileField metaField in mappedFieldsInDb)
                //{
                //    if (metaFields.Contains(metaField.MetaDataField) && !metaField.isLinked){
                //        metaField.isLinked = true;
                //    }else if (!metaFields.Contains(metaField.MetaDataField))
                //    {
                //        _context.Map_FileField.Add(new Map_FileField()
                //        {
                //            FileType = _context.Files.FirstOrDefault(c => c.Id == selectedFile.Id)
                //        });
                //    }
                //}





                //foreach(FileData meta in metaFields)
                //{
                //    if(_context.Map_FileField.Where(c => c.FileType.Id == selectedFile.Id && c.MetaDataField.Id == meta.Id).Count() > 0){
                //        _context.Map_FileField.FirstOrDefault(c => c.FileType.Id == selectedFile.Id && c.MetaDataField.Id == meta.Id).isLinked = true;
                //    }
                //    else
                //    {
                //        _context.Map_FileField.Add(new Map_FileField() {
                //            FileType = selectedFile,
                //            MetaDataField = _context.FileData.FirstOrDefault(c => c.Id == meta.Id),
                //            isLinked = true
                //        });
                //    }
                //    _context.SaveChanges();
                //}
#endregion
