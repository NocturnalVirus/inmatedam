namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTable_Files_FileExtensions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "FileExtensions", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Files", "FileExtensions");
        }
    }
}
