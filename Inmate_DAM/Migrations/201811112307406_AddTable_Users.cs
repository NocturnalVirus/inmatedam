namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_Users : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        AuthenticationType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuthenticationTypes", t => t.AuthenticationType_Id)
                .Index(t => t.AuthenticationType_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "AuthenticationType_Id", "dbo.AuthenticationTypes");
            DropIndex("dbo.Users", new[] { "AuthenticationType_Id" });
            DropTable("dbo.Users");
        }
    }
}
