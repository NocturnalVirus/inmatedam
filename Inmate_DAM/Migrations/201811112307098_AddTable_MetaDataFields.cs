namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_MetaDataFields : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MetaDataFields",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MetaDataFields");
        }
    }
}
