namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTable_FileisDeleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "isDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Files", "isDeleted");
        }
    }
}
