namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTable_FileData : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FileData", "FileInformation_Id", "dbo.FileInfo");
            DropForeignKey("dbo.FileData", "FileType_Id", "dbo.Files");
            DropIndex("dbo.FileData", new[] { "FileInformation_Id" });
            DropIndex("dbo.FileData", new[] { "FileType_Id" });
            DropTable("dbo.FileData");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FileData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaseId = c.Int(),
                        InmateId = c.Int(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DOB = c.DateTime(),
                        FileInformation_Id = c.Int(),
                        FileType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.FileData", "FileType_Id");
            CreateIndex("dbo.FileData", "FileInformation_Id");
            AddForeignKey("dbo.FileData", "FileType_Id", "dbo.Files", "Id");
            AddForeignKey("dbo.FileData", "FileInformation_Id", "dbo.FileInfo", "Id");
        }
    }
}
