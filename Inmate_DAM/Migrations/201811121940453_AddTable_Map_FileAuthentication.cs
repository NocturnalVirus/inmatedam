namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_Map_FileAuthentication : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Map_FileAuthentication",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Read = c.Boolean(nullable: false),
                        Upload = c.Boolean(nullable: false),
                        Modify = c.Boolean(nullable: false),
                        Delete = c.Boolean(nullable: false),
                        AuthenticationType_Id = c.Int(),
                        File_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuthenticationTypes", t => t.AuthenticationType_Id)
                .ForeignKey("dbo.Files", t => t.File_Id)
                .Index(t => t.AuthenticationType_Id)
                .Index(t => t.File_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Map_FileAuthentication", "File_Id", "dbo.Files");
            DropForeignKey("dbo.Map_FileAuthentication", "AuthenticationType_Id", "dbo.AuthenticationTypes");
            DropIndex("dbo.Map_FileAuthentication", new[] { "File_Id" });
            DropIndex("dbo.Map_FileAuthentication", new[] { "AuthenticationType_Id" });
            DropTable("dbo.Map_FileAuthentication");
        }
    }
}
