namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_Map_FileFields : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Map_FileField",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        File_Id = c.Int(),
                        MetaFieldsId_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.File_Id)
                .ForeignKey("dbo.MetaDataFields", t => t.MetaFieldsId_Id)
                .Index(t => t.File_Id)
                .Index(t => t.MetaFieldsId_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Map_FileField", "MetaFieldsId_Id", "dbo.MetaDataFields");
            DropForeignKey("dbo.Map_FileField", "File_Id", "dbo.Files");
            DropIndex("dbo.Map_FileField", new[] { "MetaFieldsId_Id" });
            DropIndex("dbo.Map_FileField", new[] { "File_Id" });
            DropTable("dbo.Map_FileField");
        }
    }
}
