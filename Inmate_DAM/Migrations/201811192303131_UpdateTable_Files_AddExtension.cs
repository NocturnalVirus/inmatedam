namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTable_Files_AddExtension : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "FileExtension_Id", c => c.Int());
            CreateIndex("dbo.Files", "FileExtension_Id");
            AddForeignKey("dbo.Files", "FileExtension_Id", "dbo.FileExtensions", "Id");
            DropColumn("dbo.Files", "FileExtensions");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Files", "FileExtensions", c => c.String());
            DropForeignKey("dbo.Files", "FileExtension_Id", "dbo.FileExtensions");
            DropIndex("dbo.Files", new[] { "FileExtension_Id" });
            DropColumn("dbo.Files", "FileExtension_Id");
        }
    }
}
