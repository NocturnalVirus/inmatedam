namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_FileData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaseId = c.Int(nullable: false),
                        InmateId = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DOB = c.DateTime(nullable: false),
                        FileInformation_Id = c.Int(),
                        FileType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FileInfo", t => t.FileInformation_Id)
                .ForeignKey("dbo.Files", t => t.FileType_Id)
                .Index(t => t.FileInformation_Id)
                .Index(t => t.FileType_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FileData", "FileType_Id", "dbo.Files");
            DropForeignKey("dbo.FileData", "FileInformation_Id", "dbo.FileInfo");
            DropIndex("dbo.FileData", new[] { "FileType_Id" });
            DropIndex("dbo.FileData", new[] { "FileInformation_Id" });
            DropTable("dbo.FileData");
        }
    }
}
