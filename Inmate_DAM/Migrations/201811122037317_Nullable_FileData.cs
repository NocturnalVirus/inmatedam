namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nullable_FileData : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FileData", "CaseId", c => c.Int());
            AlterColumn("dbo.FileData", "InmateId", c => c.Int());
            AlterColumn("dbo.FileData", "DOB", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FileData", "DOB", c => c.DateTime(nullable: false));
            AlterColumn("dbo.FileData", "InmateId", c => c.Int(nullable: false));
            AlterColumn("dbo.FileData", "CaseId", c => c.Int(nullable: false));
        }
    }
}
