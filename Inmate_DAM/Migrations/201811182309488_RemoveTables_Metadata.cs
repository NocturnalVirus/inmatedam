namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTables_Metadata : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Map_FileField", "File_Id", "dbo.Files");
            DropForeignKey("dbo.Map_FileField", "MetaFieldsId_Id", "dbo.MetaDataFields");
            DropIndex("dbo.Map_FileField", new[] { "File_Id" });
            DropIndex("dbo.Map_FileField", new[] { "MetaFieldsId_Id" });
            DropTable("dbo.Map_FileField");
            DropTable("dbo.MetaDataFields");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MetaDataFields",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Map_FileField",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        File_Id = c.Int(),
                        MetaFieldsId_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Map_FileField", "MetaFieldsId_Id");
            CreateIndex("dbo.Map_FileField", "File_Id");
            AddForeignKey("dbo.Map_FileField", "MetaFieldsId_Id", "dbo.MetaDataFields", "Id");
            AddForeignKey("dbo.Map_FileField", "File_Id", "dbo.Files", "Id");
        }
    }
}
