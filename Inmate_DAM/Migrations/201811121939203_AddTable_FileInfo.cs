namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_FileInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        UploadDate = c.DateTime(nullable: false),
                        FullPath = c.String(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FileInfo", "User_Id", "dbo.Users");
            DropIndex("dbo.FileInfo", new[] { "User_Id" });
            DropTable("dbo.FileInfo");
        }
    }
}
