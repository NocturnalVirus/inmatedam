namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTable_Map_FileData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Map_FileData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        FileData_Id = c.Int(),
                        FileInfo_Id = c.Int(),
                        FileType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FileData", t => t.FileData_Id)
                .ForeignKey("dbo.FileInfo", t => t.FileInfo_Id)
                .ForeignKey("dbo.Files", t => t.FileType_Id)
                .Index(t => t.FileData_Id)
                .Index(t => t.FileInfo_Id)
                .Index(t => t.FileType_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Map_FileData", "FileType_Id", "dbo.Files");
            DropForeignKey("dbo.Map_FileData", "FileInfo_Id", "dbo.FileInfo");
            DropForeignKey("dbo.Map_FileData", "FileData_Id", "dbo.FileData");
            DropIndex("dbo.Map_FileData", new[] { "FileType_Id" });
            DropIndex("dbo.Map_FileData", new[] { "FileInfo_Id" });
            DropIndex("dbo.Map_FileData", new[] { "FileData_Id" });
            DropTable("dbo.Map_FileData");
        }
    }
}
