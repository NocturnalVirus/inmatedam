namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTable_Map_FileFieldisLinked : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Map_FileField", "isLinked", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Map_FileField", "isLinked");
        }
    }
}
