namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Map_FileField : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Map_FileField",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileType_Id = c.Int(),
                        MetaDataField_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.FileType_Id)
                .ForeignKey("dbo.FileData", t => t.MetaDataField_Id)
                .Index(t => t.FileType_Id)
                .Index(t => t.MetaDataField_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Map_FileField", "MetaDataField_Id", "dbo.FileData");
            DropForeignKey("dbo.Map_FileField", "FileType_Id", "dbo.Files");
            DropIndex("dbo.Map_FileField", new[] { "MetaDataField_Id" });
            DropIndex("dbo.Map_FileField", new[] { "FileType_Id" });
            DropTable("dbo.Map_FileField");
        }
    }
}
