namespace Inmate_DAM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveTypetoFileInfo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Map_FileData", "FileType_Id", "dbo.Files");
            DropIndex("dbo.Map_FileData", new[] { "FileType_Id" });
            AddColumn("dbo.FileInfo", "FileType_Id", c => c.Int());
            CreateIndex("dbo.FileInfo", "FileType_Id");
            AddForeignKey("dbo.FileInfo", "FileType_Id", "dbo.Files", "Id");
            DropColumn("dbo.Map_FileData", "FileType_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Map_FileData", "FileType_Id", c => c.Int());
            DropForeignKey("dbo.FileInfo", "FileType_Id", "dbo.Files");
            DropIndex("dbo.FileInfo", new[] { "FileType_Id" });
            DropColumn("dbo.FileInfo", "FileType_Id");
            CreateIndex("dbo.Map_FileData", "FileType_Id");
            AddForeignKey("dbo.Map_FileData", "FileType_Id", "dbo.Files", "Id");
        }
    }
}
