﻿using Inmate_DAM.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inmate_DAM
{
    public partial class AddCategory : Form
    {
        #region Drag-Drop
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        Context _context;
        public FileManagement fileManagement;

        List<string> authTypes = new List<string>() { "Read", "Upload", "Modify", "Delete" };

        public AddCategory()
        {
            InitializeComponent();
            _context = new Context();
            lbxCategories.DisplayMember = "Name";
        }

        private void AddCategory_Load(object sender, EventArgs e)
        {
            var fileData = _context.FileExtensions.ToList();

            foreach(var meta in fileData){
                lbxCategories.Items.Add(meta);
            }

            lbxCategories.SelectedIndex = 0;

            foreach(var auth in authTypes)
            {
                if (auth != "Delete" && auth != "Modify")
                {
                    chlbx_General.Items.Add(auth, true);
                }
                else
                {
                    chlbx_General.Items.Add(auth, false);
                }


                if (auth != "Delete")
                {
                    chlbx_Manager.Items.Add(auth, true);
                }
                else
                {
                    chlbx_Manager.Items.Add(auth);
                }

                chlbx_Admin.Items.Add(auth, true);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbxCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedType = lbxCategories.SelectedItem as FileExtension;

            var extensions = selectedType.Extensions.Split(new char[] { ';' });

            tbxExtensions.Text = "";

            foreach (var extension in extensions)
            {
                tbxExtensions.Text += $"{extension} ";
            }


        }

        private void btnUpdateCategory_Click(object sender, EventArgs e)
        {
            var selectedExtension = lbxCategories.SelectedItem as FileExtension;
            var extensionInDb = _context.FileExtensions.FirstOrDefault(ex => ex.Id == selectedExtension.Id);

            var file = new File() { FileBasePath = tbxPathName.Text, FileExtension = extensionInDb, Name = tbxCategoryName.Text };

            //Authorization

            var GeneralAuth = new Map_FileAuthentication() { AuthenticationType = _context.AuthenticationTypes.FirstOrDefault(a => a.Id == 1) };
            var ManagerAuth = new Map_FileAuthentication() { AuthenticationType = _context.AuthenticationTypes.FirstOrDefault(a => a.Id == 2) };
            var AdminAuth = new Map_FileAuthentication() { AuthenticationType = _context.AuthenticationTypes.FirstOrDefault(a => a.Id == 3) };

            foreach(var chkBox in chlbx_General.Items)
            {
                var index = chlbx_General.Items.IndexOf(chkBox);

                if ((chkBox as string) == "Read")
                {
                    GeneralAuth.Read = chlbx_General.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Upload")
                {
                    GeneralAuth.Upload = chlbx_General.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Modify")
                {
                    GeneralAuth.Modify = chlbx_General.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Delete")
                {
                    GeneralAuth.Delete = chlbx_General.GetItemChecked(index);
                }
            }

            foreach (var chkBox in chlbx_Manager.Items)
            {
                var index = chlbx_Manager.Items.IndexOf(chkBox);

                if ((chkBox as string) == "Read")
                {
                    ManagerAuth.Read = chlbx_Manager.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Upload")
                {
                    ManagerAuth.Upload = chlbx_Manager.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Modify")
                {
                    ManagerAuth.Modify = chlbx_Manager.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Delete")
                {
                    ManagerAuth.Delete = chlbx_Manager.GetItemChecked(index);
                }
            }

            foreach (var chkBox in chlbx_Admin.Items)
            {
                var index = chlbx_Admin.Items.IndexOf(chkBox);

                if ((chkBox as string) == "Read")
                {
                    AdminAuth.Read = chlbx_Admin.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Upload")
                {
                    AdminAuth.Upload = chlbx_Admin.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Modify")
                {
                    AdminAuth.Modify = chlbx_Admin.GetItemChecked(index);
                }
                else if ((chkBox as string) == "Delete")
                {
                    AdminAuth.Delete = chlbx_Admin.GetItemChecked(index);
                }
            }

            _context.Files.Add(file);
            GeneralAuth.File = file;
            ManagerAuth.File = file;
            AdminAuth.File = file;

            _context.Map_FileAuthentication.Add(GeneralAuth);
            _context.Map_FileAuthentication.Add(ManagerAuth);
            _context.Map_FileAuthentication.Add(AdminAuth);

            _context.SaveChanges();

            fileManagement.ToggleShowDeleted();
            fileManagement._mainForm.FillCategories();
            this.Close();


            //General
            //_context.Map_FileAuthentication.Add();
        }
    }
}
