﻿using Inmate_DAM.Models;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Inmate_DAM
{
    public partial class FileImport : Form
    {
        #region Drag-Drop
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        Context _context;
        public Form1 _form;
        public string path = "";
        public string filePath = "";
        public string fileName = "";

        public FileImport()
        {
            InitializeComponent();

            _context = new Context();
        }

        public FileImport(StreamReader filestream)
        {
            InitializeComponent();
            _context = new Context();
            string extension = "";

            if (filestream.BaseStream is FileStream)
            {
                var localFilepath = (filestream.BaseStream as FileStream).Name;
                fileName = localFilepath.Substring(localFilepath.LastIndexOf('\\') + 1);
                extension = localFilepath.Substring(localFilepath.LastIndexOf('.'));

                filePath = localFilepath;

                lblFileName.Text = fileName;
                pbxFileThumbnail.SizeMode = PictureBoxSizeMode.Zoom;

                if (ImportFileTypes(extension))
                {
                    var categories = _context.Files.Include(c => c.FileExtension).Where(c => c.FileExtension.Extensions.Contains(extension) && !c.isDeleted).ToList();
                    lblExtnType.Text = categories.FirstOrDefault().FileExtension.Name;

                    if (categories.FirstOrDefault().FileExtension.Name == "Document")
                    {
                        pbxFileThumbnail.Image = Inmate_DAM.Properties.Resources.img_document;
                    }else if(categories.FirstOrDefault().FileExtension.Name == "Video")
                    {
                        pbxFileThumbnail.Image = Inmate_DAM.Properties.Resources.img_Video;
                    }

                    cbxFileType.DataSource = categories;

                    localFilepath.Substring(localFilepath.LastIndexOf('\\') + 1);
                }
                else
                {
                    //DialogResult dialog = MessageBox.Show(null, $"extension '{extension}' is not assigned to a file type\r\n\r\nWould you like to open the File Manager?", "Invalid File Extension", MessageBoxButtons.YesNo);
                    //if(dialog == DialogResult.Yes)
                    //{
                    //    MessageBox.Show("You clicked Yes!");
                    //}
                    MessageBox.Show(null, $"extension '{extension}' is not assigned to a file type", "Invalid File Extension");
                    filestream.Dispose();
                    this.Close();
                }
            }
        }

        private bool ImportFileTypes(string extension)
        {
            if (_context.Files.Include(c => c.FileExtension).Where(c => c.FileExtension.Extensions.Contains(extension)).Count() > 0)
            {
                return true;
            }
            return false;
        }

        private void cbxFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tbx_y = 40;
            var lbl_y = 25;

            var selectedCategory = cbxFileType.SelectedItem as Inmate_DAM.Models.File;
            lblFileType.Text = selectedCategory.Name;

            //lblPath.Text = filepath;
            lblPath.Text = $"\\storage\\{selectedCategory.FileBasePath}\\{lblFileName.Text}";
            path = $"\\storage\\{selectedCategory.FileBasePath}\\";

            var metaData = _context.Map_FileField.Include(c => c.FileType).Include(c => c.MetaDataField).Where(c => c.FileType.Name == selectedCategory.Name && c.isLinked == true).ToList();

            gbxMetaData.Controls.Clear();

            foreach(var data in metaData)
            {
                gbxMetaData.Controls.Add(new TextBox() { Name = $"tbx_{data.MetaDataField.Name}", Location = new Point(10, tbx_y), Width = 245 });
                gbxMetaData.Controls.Add(new Label() { Text = $"{data.MetaDataField.Name}:", Location = new Point(9, lbl_y) });

                tbx_y += 40;
                lbl_y += 40;
            }
        }

        private void FileImport_Load(object sender, EventArgs e)
        {

        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            List<string> metaFields = new List<string>();
            Map_FileData map_FileData = null;

            var currCategory = cbxFileType.SelectedItem as Models.File;
            var fileInfo = new Models.FileInfo()
            {
                FileName = lblFileName.Text,
                FileType = currCategory,
                UploadDate = DateTime.Now,
                User = _context.Users.FirstOrDefault(s => s.Id == Session.currentUser.Id),
                FullPath = lblPath.Text
            };


            foreach (Control control in gbxMetaData.Controls)
            {
                if (control.Name.Contains("tbx_"))
                {
                    var friendlyName = control.Name.Substring(control.Name.LastIndexOf('_') + 1);
                    map_FileData = new Map_FileData()
                    {
                        FileData = _context.FileData.FirstOrDefault(c => c.Name == friendlyName),
                        FileInfo = fileInfo,
                        Value = control.Text
                    };

                        _context.Map_FileData.Add(map_FileData);
                        _context.SaveChanges();
                }
            }

            bool exists = System.IO.Directory.Exists(Environment.CurrentDirectory + path);

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Environment.CurrentDirectory + path);
            }

            System.IO.File.Copy(filePath, Environment.CurrentDirectory + path + fileName, true);

            _form.FillCategories();
            _form.lbxCategories.SelectedIndex = 0;

            this.Close();
        }

        private void pnlDrag_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNewFile_Click(object sender, EventArgs e)
        {
            _form.FileImport(_form, e);
            this.Close();
        }
    }
}
