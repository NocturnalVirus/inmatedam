﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    [Table("FileData")]
    public class FileData
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
