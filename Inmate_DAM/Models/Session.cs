﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inmate_DAM.Models
{
    public static class Session
    {
        public static User currentUser { get; set; }
        public static Form1 mainForm { get; set; }

        public static void MainForm(Login form)
        {
            var Form1 = new Form1();
            mainForm = Form1;
            Form1.loginForm = form;
            Form1.Show();
        }
    }
}
