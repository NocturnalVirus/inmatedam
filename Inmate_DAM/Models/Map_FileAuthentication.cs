﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    public class Map_FileAuthentication
    {
        public int Id { get; set; }
        [Key, Column(Order = 0)]
        public File File { get; set; }
        [Key, Column(Order = 1)]
        public AuthenticationType AuthenticationType { get; set; }
        public bool Read { get; set; }
        public bool Upload { get; set; }
        public bool Modify { get; set; }
        public bool Delete { get; set; }
    }
}
