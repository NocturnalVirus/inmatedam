﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    class Map_FileField
    {
        public int Id { get; set; }
        [Key, Column(Order = 0)]
        public File FileType { get; set; }
        [Key, Column(Order = 1)]
        public FileData MetaDataField { get; set; }
        public bool isLinked { get; set; }
    }
}
