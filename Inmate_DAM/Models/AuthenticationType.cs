﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    public class AuthenticationType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
