﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    class Context : DbContext
    {
        public DbSet<File> Files { get; set; }
        public DbSet<AuthenticationType> AuthenticationTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<FileInfo> FileInformation { get; set; }
        public DbSet<Map_FileAuthentication> Map_FileAuthentication { get; set; }
        public DbSet<FileData> FileData { get; set; }
        public DbSet<Map_FileData> Map_FileData { get; set; }
        public DbSet<Map_FileField> Map_FileField { get; set; }
        public DbSet<FileExtension> FileExtensions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<Context>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
