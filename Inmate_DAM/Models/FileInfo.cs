﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    [Table("FileInfo")]
    public class FileInfo
    {
        public int Id { get; set; }
        [Key, Column(Order = 0)]
        public User User { get; set; }
        public string FileName { get; set; }
        public DateTime UploadDate { get; set; }
        public string FullPath { get; set; }
        [Key, Column(Order = 1)]
        public File FileType { get; set; }

        public override string ToString()
        {
            return $"{FileName}";
        }
    }
}
