﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    public class File
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FileBasePath { get; set; }
        public FileExtension FileExtension { get; set; }
        public bool isDeleted { get; set; }
    }
}
