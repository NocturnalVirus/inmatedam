﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inmate_DAM.Models
{
    [Table("Map_FileData")]
    public class Map_FileData
    {
        public int Id { get; set; }
        [Key, Column(Order = 0)]
        //public int FileInfoId { get; set; }
        public FileInfo FileInfo { get; set; }
        [Key, Column(Order = 1)]
        public FileData FileData { get; set; }
        public string Value { get; set; }
    }
}
